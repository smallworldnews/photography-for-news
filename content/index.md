# Photography for News

![](images/PN_1.jpg)

---

The Internet is a visual environment and the individual photograph is among its most basic units of content. Photos for news should document the scene or subject of your story in a way that allows people to see and understand its significance.

Photographs are the first things people look at in a news story in print, on a computer screen, or a mobile device. Photographs are are universal -- you do not need to speak a specific language to understand them.

A single image can convey more information about a scene than even several paragraphs of text -- a picture is worth a thousand words. Most importantly, good photography capture the shared human drama of important events in a way that allows the viewer to empathize with the people pictured. At the same time, the best photography is faithful to the truth of the event portrayed.

Cameras in mobile devices are getting better and better. Mobile devices put the power of photography in the hands of ever-greater numbers of people. The proliferation of mobile photo also raises expectations of viewers. Today’s news consumer is visually sophisticated. To be successful individual journalists and the news outlets they represent must provide excellent photos to news consumers. StoryMaker, which can be used on any Android device, is designed help you produce and publish strong photos quickly.

Photography is a straightforward way to introduce a visual component into every text story and StoryMaker will help you shoot and publish your images quickly. Your organization may also opt to do stories that are primarily photo in the form of photo essays. Moving forward, file photos and stock photos -- images of landscapes, tourist destinations and important public figures and important cultural events -- could become a strong source of revenue for your news organization.

At the very least every news story should have a photo that illustrates what the story is about. A news photo should portray the people and location in clear, compelling way. Some social platforms have a ‘photo-first’ strategy that emphasizes the visual elements of a story, along with tags, keywords and a bit of contextual information, over text. In addition to simply relaying the news, photos do several other things for a publication: Good photos make page layout more appealing. Photos drive traffic to your site and photos win awards. In addition to the single news photo, news organizations produce photo essays when a story is especially visual. Additionally, photos play a role in multimedia stories and mapping projects. Photos work as graphic ‘pointers’ to other content in the form of thumbnails.

The following guidelines for taking compelling and informative pictures are intended to be studied with StoryMaker in hand. While reading this tutorial, explore StoryMaker. Take some pictures, edit and upload them. Get a feel for photography and the app.

---

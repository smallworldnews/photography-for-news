# 5. Ethics and News Photography

Below are the ethical guidelines published by **National Press Photographers Association** in the United States. It is much like professional and ethical guidelines published internationally by similar organizations.

As journalists we believe the guiding principle of our profession is accuracy; therefore, we believe it is wrong to alter the content of a photograph in any way that deceives the public.

As photojournalists, we have the responsibility to document society and to preserve its images as a matter of historical record. It is clear that the emerging electronic technologies provide new challenges to the integrity of photographic images... in light of this, we the National Press Photographers Association, reaffirm the basis of our ethics: Accurate representation is the benchmark of our profession. We believe photojournalistic guidelines for fair and accurate reporting should be the criteria for judging what may be done electronically to a photograph. Altering the editorial content... is a breach of the ethical standards recognized by the NPPA.


1. Be accurate and comprehensive in the representation of subjects.

2. Resist being manipulated by staged photo opportunities.

3. Be complete and provide context when photographing or recording subjects. Avoid stereotyping individuals and groups. Recognize and work to avoid presenting one’s own biases in the work.

4. Treat all subjects with respect and dignity. Give special consideration to vulnerable subjects and compassion to victims of crime or tragedy. Intrude on private moments of grief only when the public has an overriding and justifiable need to see.

5. While photographing subjects do not intentionally contribute to, alter, or seek to alter or influence events.

6. Editing should maintain the integrity of the photographic images’ content and context. Do not manipulate images or add or alter sound in any way that can mislead viewers or misrepresent subjects.

---

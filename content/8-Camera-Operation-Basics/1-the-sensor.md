# 8.1 The Sensor

![](images/sensor-overview.png)

| Letter | Description                          |
|:------:|:-------------------------------------|
|  A     | Incoming Light                       |
|  B     | Color Filters                        |
|  C     | Light is converted to electricity    |
|  D     | Sensor is made up of millions of pixels that collects light |

---

## ISO

Digital sensors can be set at different record levels, increasing or decreasing their sensitivity to light. This system for adjusting the sensor’s sensitivity is called the ISO and is measured in numeric range, typically from 100 to 800 and higher on smartphones.

![](images/ISO-diagram.png)

Your ISO should be higher in low light (800) and lower in bright light (100). If you can manually set the ISO on your camera, you should.

---

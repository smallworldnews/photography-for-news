# 8.3 Aperture

![](images/aperture-diagram.png)

The aperture is a small iris in the lens of a camera. The larger the valve opening, the more light that will strike the sensor. The smaller the iris valve, the less light striking the sensor. The aperture also controls depth-of-field -- the area in front of and behind the subject that is in focus. The larger the aperture, the shallower the depth of field. In some lenses and cameras the range of aperture size available will change depending on how much you zoom in or out.

Creating an image with a proper exposure is very much a game of playing ISO, aperture and shutter o  of one another to achieve the desired effect. The bigger the camera the more manual control the photographer will have over these features, but even smart phone cameras now will let you adjust exposure by touching the screen at a point that represents a medium expo- sure value. Some also have digital zoom that, in effect, allows you to zoom in or out. This is called the exposure triangle.

Cameras decide exposure by reading of the amount of light striking the sensor and then averaging it based on a set standard. The problem is that cameras do not do a good job at accounting for unusual lighting or the special needs of photojournalists. The camera will usually get you close, but only close. You need to manage it.

---

# 4. Troubleshooting

Mobile cameras and small point-and-shoots have small maximum apertures, which make the camera hard to use in low light. When thinking through the reporting of your story, keep this in mind. You cannot always control when stories happen but if you can choose time of day for maximum light that will make a big difference in your photos. If there is little or no light because you are indoors, do not be afraid to turn on lights or open curtains. For portraits and some feature stories you may be able to move your subject to a location with better light.

![](images/PN_10.jpg)

Perhaps the biggest mistake made by beginning photographers is to not get close enough to your subject.

This typically happens for one of two reasons:

1. the photographer is too frightened to get close to the subject. Editors call these these kind of photos intimidation shots.

2. The photographer looks through the camera viewfinder or at the monitor and thinks he is close to the subject but is not filling the frame with only his subject and related contextual information.

---

![](images/PN_11.jpg)

Robert Capa, a famous Hungarian-American photographer, expressed it best when he said, **“If your images aren’t good enough, you are not close enough.”** Get close to your subject. Fill the frame with important information and only important information. Don’t just use the center of the frame. Look at the edges of the picture and make sure that there is no wasted space. Capa was also talking about the photographer’s emotional proximity to the subject. If the photographer isn’t passionate about the subject, it will be obvious in the image. If the photographer does not care, why should the viewer?

No image is worth injury or death. If you cannot get close because it is unsafe, don’t. See if you can compose your image differently or perhaps crop it in post production.

---
